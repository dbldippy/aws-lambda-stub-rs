#!/bin/bash

# Starts and stops EC2 instances which don't have an elastic IP. 
#
# The script starts or stops instances, patches the SSH config with the new
# public IP, and resets an alarm that can be used to stop the instance when
# idle. 
#
# The instance must be configured in .ssh/config in the following way:
#
# Host <hostname> # <instance-id>
#     HostName 18.220.145.215 # <instance-id> <alarm-name>
#     IdentityFile <..>
#     User <..>

set -e

ssh_config=~/.ssh/config
known_hosts=~/.ssh/known_hosts

function adjust_ssh_config() {
    local id=$1
    shift

    # find the current instance ip using a label
    curr_ip=$(gawk "/HostName.*${id}/ {print \$2}" $ssh_config)
    echo current IP: $curr_ip

    # query the new instance ip
    while
        new_ip=$(aws ec2 describe-instances --instance-ids $id | gawk -F\" '/PublicIpAddress/ {print $4}')
        echo querying new IP...
        [[ -z $new_ip ]]
    do true; done
    echo new IP: $new_ip

    # replace the current ip with the new ip in the ssh config
    echo replacing IP in $ssh_config
    sed -i "s/$curr_ip/$new_ip/" $ssh_config
    # ...and in the list of known hosts
    echo replacing IP in $known_hosts
    sed -i "s/$curr_ip/$new_ip/" $known_hosts
}

function reset_stop_alarm() {
    local id=$1
    shift

    alarm=$(gawk "/HostName.*${id}/ {print \$5}" $ssh_config)
    echo resetting stop alarm $alarm
    aws cloudwatch set-alarm-state --alarm-name $alarm --state-value OK --state-reason "instance start"
}

function start() {
    local id=$1
    shift

    echo starting instance $id
    aws ec2 start-instances --instance-id $id
    adjust_ssh_config $id
    reset_stop_alarm $id
}

function stop() {
    local id=$1
    shift

    echo stopping instance $id
    aws ec2 stop-instances --instance-id $id
}

cmd=$1
shift

# get instance id
id=$(gawk "/Host ${hostname}.*#/ {print \$4}" $ssh_config)
shift

$cmd $id $@

# .bashrc for Amazon Linux 2

# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
source "$HOME/.cargo/env"

export AWS_ACCESS_KEY_ID=<>
export AWS_SECRET_ACCESS_KEY=<>
export AWS_DEFAULT_REGION=<>

eval "$(starship init bash)"

alias ls=exa

dir=$HOME/.config
[ -f $dir/completion.bash ] && source $dir/completion.bash
[ -f $dir/key-bindings.bash ] && source $dir/key-bindings.bash

alias bat='bat -p'
alias less=bat

alias la='ls -la'
alias c='cargo'
alias g='git'
alias s='sam'
alias d='docker'

# x86_64 musl toolchain
X86_64_TOOL_DIR=$HOME/x86_64-linux-musl
PATH=$PATH:$X86_64_TOOL_DIR/bin
export CC_x86_64_unknown_linux_musl=x86_64-linux-gcc

# openssl-devel for x86-64 musl 
export PKG_CONFIG_SYSROOT_DIR=$X86_64_TOOL_DIR
export PKG_CONFIG_PATH=$X86_64_TOOL_DIR/usr/libopenssl-1.1.1j

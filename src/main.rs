use lambda_http::{
    handler,
    lambda_runtime::{self, Context},
    IntoResponse, Request, RequestExt, Response,
};
use rusoto_core::Region;
use rusoto_ec2::{Ec2, Ec2Client, StartInstancesRequest, StopInstancesRequest};
use serde_derive::{Deserialize, Serialize};
use serde_json::json;
use simple_error::{bail, SimpleError};

type Error = Box<dyn std::error::Error + Sync + Send + 'static>;

#[tokio::main]
async fn main() -> Result<(), Error> {
    lambda_runtime::run(handler(func)).await?;
    Ok(())
}

#[derive(Debug, Serialize, Deserialize, Default)]
struct Event {
    #[serde(default)]
    state: String,
    #[serde(default)]
    instance_ids: Vec<String>,
    #[serde(default)]
    dry_run: bool,
}

async fn func(request: Request, ctx: Context) -> Result<impl IntoResponse, Error> {
    dbg!(&request);
    dbg!(&ctx);

    // try to get url-encoded parameters
    let event: Event = {
        let params = request.query_string_parameters();
        if !params.is_empty() {
            Event {
                state: params.get("state").unwrap_or_default().into(),
                instance_ids: params.get_all("instance_ids".into()).unwrap_or_default()[0] // TODO arrays seem to not be decoded correctly
                    .split(',')
                    .map(|s| s.to_string())
                    .collect(),
                dry_run: params
                    .get("dry_run")
                    .unwrap_or_default()
                    .parse()
                    .unwrap_or_default(),
            }
        } else {
            request
                .payload()
                .unwrap_or_else(|_parse_err| None)
                .unwrap_or_default()
        }
    };

    dbg!(&event);

    Ok(match transition_to(event).await {
        Err(e) => Response::builder()
            .status(400)
            .body(
                json!({ "result": format!("error: {}", e.as_str()) })
                    .to_string()
                    .into(),
            )
            .expect("failed to render response"),
        Ok(_) => json!({"result": "ok"}).into_response(),
    })
}

async fn transition_to(event: Event) -> Result<(), SimpleError> {
    let client = Ec2Client::new(Region::EuCentral1);

    match dbg!(event.state.as_str()) {
        "on" => match client
            .start_instances(StartInstancesRequest {
                additional_info: None,
                instance_ids: event.instance_ids,
                dry_run: Some(event.dry_run),
            })
            .await
        {
            Ok(_) => Ok(()),
            Err(e) => err_or_ok(e.to_string()),
        },
        "off" => match client
            .stop_instances(StopInstancesRequest {
                force: None,
                hibernate: None,
                instance_ids: event.instance_ids,
                dry_run: Some(event.dry_run),
            })
            .await
        {
            Ok(_) => Ok(()),
            Err(e) => err_or_ok(e.to_string()),
        },
        _ => bail!("state invalid"),
    }
}

fn err_or_ok(e: String) -> Result<(), SimpleError> {
    match e.contains("would have succeeded") {
        true => Ok(()),
        false => Err(SimpleError::new(e)),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use lambda_http::http::{header, HeaderValue};
    use serde_json::json;

    #[tokio::test]
    async fn happy_json() {
        for s in &["on", "off"] {
            let event = Event {
                state: format!(r#"{}"#, s),
                instance_ids: vec!["i-09d7f11fe4f28439b".to_string()],
                dry_run: true,
            };

            let mut req = Request::new(lambda_http::Body::from(
                serde_json::to_string(&event).unwrap(),
            ));
            req.headers_mut().insert(
                header::CONTENT_TYPE,
                HeaderValue::from_str("application/json").unwrap(),
            );

            assert_eq!(
                func(req, Context::default())
                    .await
                    .expect("expected Ok(_) value")
                    .into_response()
                    .body(),
                json!({"result": "ok"}).into_response().body()
            );
        }
    }

    #[tokio::test]
    async fn happy_urlencode() {
        for s in &["on", "off"] {
            let mut req = Request::default();
            *req.uri_mut() = "https://sth.com/state?state=on&instance_ids=i-09d7f11fe4f28439b&instance_ids=i-09d7f11fe4f28439b&dry_run=true".parse().unwrap();
            req.headers_mut().insert(
                header::CONTENT_TYPE,
                HeaderValue::from_str("application/x-www-form-urlencoded").unwrap(),
            );

            assert_eq!(
                func(req, Context::default())
                    .await
                    .expect("expected Ok(_) value")
                    .into_response()
                    .body(),
                json!({"result": "ok"}).into_response().body()
            );
        }
    }

    #[tokio::test]
    async fn invalid_state_1() {
        let event = Event {
            state: "hibernate".to_string(), // nice to have but currently not supported
            instance_ids: vec!["i-09d7f11fe4f28439b".to_string()],
            dry_run: true,
        };

        let mut req = Request::new(lambda_http::Body::from(
            serde_json::to_string(&event).unwrap(),
        ));
        req.headers_mut().insert(
            header::CONTENT_TYPE,
            HeaderValue::from_str("application/json").unwrap(),
        );

        assert_eq!(
            func(req, Context::default())
                .await
                .expect("expected Ok(_) value")
                .into_response()
                .body(),
            json!({"result": "error: state invalid"})
                .into_response()
                .body()
        );
    }

    #[tokio::test]
    async fn url_test() {
        use serde_qs;

        #[derive(Serialize, Deserialize, Debug)]
        struct S {
            a: String,
            b: Vec<String>,
        }
        let s = S {
            a: "aa".into(),
            b: vec!["b1".into(), "b2".into()],
        };

        let s = serde_qs::to_string(&s).unwrap();

        assert_eq!(s, "a".to_string());
    }

}
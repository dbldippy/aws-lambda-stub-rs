use log::{debug};
use rusoto_core::Region;
use rusoto_ec2::{Ec2, Ec2Client, StartInstancesRequest, StopInstancesRequest};
use serde_derive::{Deserialize, Serialize};
use simple_error::{bail, SimpleError};

#[derive(Debug, Serialize, Deserialize, Default)]
pub(crate) struct Event {
    #[serde(default)]
    state: String,
    #[serde(default)]
    instance_ids: Vec<String>,
    #[serde(default)]
    dry_run: bool,
}

pub(crate) async fn transition_to(event: Event) -> Result<(), SimpleError> {
    debug!("{:?}", &event);
    let client = Ec2Client::new(Region::EuCentral1);

    match event.state.as_str() {
        "on" => match client
            .start_instances(StartInstancesRequest {
                additional_info: None,
                instance_ids: event.instance_ids,
                dry_run: Some(event.dry_run),
            })
            .await
        {
            Ok(_) => Ok(()),
            Err(e) => err_or_ok(e.to_string()),
        },
        "off" => match client
            .stop_instances(StopInstancesRequest {
                force: None,
                hibernate: None,
                instance_ids: event.instance_ids,
                dry_run: Some(event.dry_run),
            })
            .await
        {
            Ok(_) => Ok(()),
            Err(e) => err_or_ok(e.to_string()),
        },
        _ => bail!("state invalid"),
    }
}

fn err_or_ok(e: String) -> Result<(), SimpleError> {
    match e.contains("would have succeeded") {
        true => Ok(()),
        false => Err(SimpleError::new(e)),
    }
}
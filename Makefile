build-AwsLambdaStub:
	cargo build --release --target x86_64-unknown-linux-musl
	cp ./target/x86_64-unknown-linux-musl/release/aws-lambda-stub $(ARTIFACTS_DIR)/bootstrap

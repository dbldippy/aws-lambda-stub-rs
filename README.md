# AWS Lambda Stub

Minimal code for a AWS Lambda function written in Rust. Develop on an EC2 arm64 build instance.

- EC2 arm64 instance as build host
- Amazon Linux 2
- SAM CLI
- latest stable Rust

## Prerequisites

### Development Environment

- create and run Amazon Linux 2 ARM64 t4g.small EC2 instance
- setup ssh access and connect with VSCode
- install and configure Git:
```
sudo yum install git
git config --global user.name "your name"
git config --global user.email "your@email.ad.dr"
git config --global pull.rebase true
```
- install rustup
- add the rustup path to the remote VSCode settings (see below)
- install VSCode Rust extension
- open a .rs file to install additional Rust modules
- optionally, install shell convenience


### X86_64 Musl Toolchain

Build a GCC toolchain for x86_64 with Buildroot:

1. Follow [Buildroot quick start](https://buildroot.org/downloads/manual/manual.html#_buildroot_quick_start) to install Buildroot
1. Configure and build the target toolchain:
```
sudo yum install gcc gcc-c++ patch ncurses-devel perl-Data-Dumper perl-Thread-Queue
make menuconfig 
curl -OL https://buildroot.org/downloads/buildroot-2021.02.tar.gz
tar xvf buildroot-2021.02.tar.gz 
cd buildroot-<>
make menuconfig
# target options->target arch: x86_64
# toolchain->C lib: musl
# toolchain->C++ support
# target packages->libs->crypto: openssl
make
```
1. Install the toolchain:
```
mkdir ~/x86_64-linux-musl
mv output/host/* ~/x86_64-linux-musl
mv output/build/libopenssl-1.1.1j ~/x86_64-linux-musl
make distclean
```
1. Install and configure the Rust musl target
```
rustup target add x86_64-unknown-linux-musl
printf '[target.x86_64-unknown-linux-musl]\nlinker = "x86_64-linux-ld"\n\n' >> ~/.cargo/config
```

### Shell Convenience (Optional)

```
# compile and install CLI tools
cargo install cargo-update starship ripgrep skim fd-find exa bat
mkdir ~/.config
cp starship.toml ~/.config
```

### SAM CLI for ARM64

```
sudo amazon-linux-extras install python3.8
sudo yum install python38-devel
sudo pip3.8 install aws-sam-cli
```

# Build and Install

Development (compiled for host):

```
cargo t
```

Development (compiled for target)

sudo yum groupinstall "Development Tools"
```
cargo b --target x86_64-unknown-linux-musl
```

Production:
```
sam build && sam deploy --guided
```

## Resources

[Building Custom Runtimes](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/building-custom-runtimes.html)

[aws/aws-sam-cli-app-templates](https://github.com/aws/aws-sam-cli-app-templates)

https://github.com/awslabs/aws-lambda-rust-runtime
[feat: support rust via cargo](https://github.com/aws/aws-lambda-builders/pull/174)

[Please add an example for Rust lambda runtime #167](https://github.com/aws/aws-lambda-builders/issues/167)

## Examples

[Rust Runtime for AWS Lambda](https://aws.amazon.com/blogs/opensource/rust-runtime-for-aws-lambda/)

[AWS Lambda + Rust](https://dev.to/rad_val_/aws-lambda-rust-292g)

[example using lambda_http](https://github.com/valentinradu/rust-aws-lambda-example/blob/master/src/article.rs)

## Issues

### Segmentation Fault When Executing Request

When compiling for musl, every sys dependency has to be build against musl, inluding e.g. the often-used OpenSSL. A gdb backtrace revealed that rusoto had trouble sending a request. 

To fix this, I cut the dependency to OpenSSL by using feature `rustls` for `rusoto_core` and `rusoto_ec2`, as described in [Using rusoto in Lambdas](https://rusoto.org/lambdas.html).


### Vscode: 'rustup not found', 'could not start language server'

Add the following to the remote settings in `/home/ec2-user/.vscode-server/data/Machine/settings.json`:

```
"rust-client.rustupPath": "/home/ec2-user/.cargo/bin/rustup"
```
